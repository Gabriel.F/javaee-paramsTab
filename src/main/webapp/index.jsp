<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>ParamsIntoJSP</title>
    <link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
    <h1>Index</h1>
    <table>
        <c:forEach var="row" items="${paramValues}">
            <tr>
                <td><c:out value="${row.key}"/></td>
                <td><c:out value="${fn:join(row.value, \", \")}"/></td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
